package powell.com.qualcarro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import powell.com.fragments.DialogFragmentCalculadora;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView textViewResultado;
    private EditText editTextNumero;
    private Button buttonSomar;
    private Button buttonDiminuir;
    private Button buttonMultiplicar;
    private Button buttonDividir;
    private Button buttonResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        textViewResultado = (TextView) findViewById(R.id.textViewResultado);
        editTextNumero = (EditText) findViewById(R.id.editTextNumero);
        buttonDiminuir = (Button) findViewById(R.id.buttonDiminuir);
        buttonDividir = (Button) findViewById(R.id.buttonDividir);
        buttonMultiplicar = (Button) findViewById(R.id.buttonMultiplicar);
        buttonSomar = (Button) findViewById(R.id.buttonSomar);
        buttonResultado = (Button) findViewById(R.id.buttonIgual);






        DialogFragmentCalculadora dialogFragmentCalculadora = new DialogFragmentCalculadora();
        dialogFragmentCalculadora.show(getSupportFragmentManager(),"Calculadora");
    }
}

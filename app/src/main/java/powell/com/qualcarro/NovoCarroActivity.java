package powell.com.qualcarro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import powell.com.dao.CarroDao;
import powell.com.model.Carro;

public class NovoCarroActivity extends AppCompatActivity {

    private Spinner spinnerFabricante;
    private EditText editTextModelo;
    private EditText editTextAno;
    private EditText editTextValor;
    private CheckBox checkAirBag;
    private CheckBox checkBoxAbs;
    private CheckBox checkBoxRoda;
    private CheckBox checkBoxPintura;
    private CheckBox checkBoxDirecao;
    private CheckBox checkBoxAr;
    private Button buttonSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_carro);

        spinnerFabricante = (Spinner) findViewById(R.id.spinnerFabricante);
        editTextModelo = (EditText) findViewById(R.id.editTextModelo);
        editTextAno = (EditText) findViewById(R.id.editTextAno);
        editTextValor = (EditText) findViewById(R.id.editTextValor);
        editTextValor = (EditText) findViewById(R.id.editTextValor);
        checkAirBag = (CheckBox) findViewById(R.id.checkboxAirBag);
        checkBoxAbs = (CheckBox) findViewById(R.id.checkboxAbs);
        checkBoxPintura = (CheckBox) findViewById(R.id.checkboxPintura);
        checkBoxAr = (CheckBox) findViewById(R.id.checkboxArCondicionado);
        checkBoxDirecao = (CheckBox) findViewById(R.id.checkboxDirecao);
        checkBoxRoda = (CheckBox) findViewById(R.id.checkboxRodas);
        buttonSalvar = (Button) findViewById(R.id.buttonSalvar);

        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fabricante = (String) spinnerFabricante.getSelectedItem();
                String modelo = editTextModelo.getEditableText().toString();
                String ano = editTextAno.getEditableText().toString();
                boolean temAirBag = checkAirBag.isChecked();
                boolean temAbs = checkBoxAbs.isChecked();
                boolean temDirecao = checkBoxDirecao.isChecked();
                boolean temAr = checkBoxAr.isChecked();
                boolean temRoda = checkBoxRoda.isChecked();
                boolean temPintura = checkBoxPintura.isChecked();
                Carro novoCarro = new Carro();
                novoCarro.setFabricante(fabricante);
                novoCarro.setRoda(temRoda);
                novoCarro.setPintura(temPintura);
                novoCarro.setDirecao(temDirecao);
                novoCarro.setAbs(temAbs);
                novoCarro.setAirbag(temAirBag);
                novoCarro.setAr(temAr);
                novoCarro.setAno(ano);
                novoCarro.setModelo(modelo);
                novoCarro.setValor(editTextValor.getText().toString());

                if (validaCampos(novoCarro)) {
                    adiciona(novoCarro);
                    limpaTela();

                }

            }
        });

    }

    private void adiciona(Carro novoCarro) {
        CarroDao carroDao = new CarroDao(getApplicationContext());
        carroDao.adiciona(novoCarro);
        Toast.makeText(getApplicationContext(), "Obrigado por adicionar um carro! " + novoCarro, Toast.LENGTH_LONG).show();
    }

    private boolean validaCampos(Carro carro) {
        if(carro.getFabricante().equals("")){
            editTextModelo.setError(getString(R.string.campo_branco));
            return false;
        }

        if(carro.getModelo().equals("")){
            editTextModelo.setError(getString(R.string.campo_branco));
            return false;
        }

        if(carro.getAno() == 0){
            editTextAno.setError(getString(R.string.campo_branco));
            return false;
        }

        if(carro.getValor() == null){
            editTextAno.setError(getString(R.string.campo_branco));
            return false;
        }

        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_novo_carro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_limpar) {
            limpaTela();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void limpaTela() {
        spinnerFabricante.setSelection(0);
        editTextModelo.setText("");
        editTextAno.setText("");
        editTextValor.setText("");
        checkBoxAbs.setChecked(false);
        checkAirBag.setChecked(false);
        checkBoxRoda.setChecked(false);
        checkBoxDirecao.setChecked(false);
        checkBoxPintura.setChecked(false);
        checkBoxAr.setChecked(false);
    }
}

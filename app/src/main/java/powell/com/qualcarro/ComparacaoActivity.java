package powell.com.qualcarro;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import powell.com.dao.CarroDao;
import powell.com.fragments.DataFragment;
import powell.com.fragments.ResultadoDialogFragment;
import powell.com.model.Carro;
import powell.com.model.Comparacao;
import powell.com.model.Ponto;
import powell.com.model.SpinnerComparacao;
import powell.com.padroes.SpinnerFactory;
import powell.com.utils.ConstantsUtils;
import powell.com.utils.DateUtils;

public class ComparacaoActivity extends AppCompatActivity {

    private EditText editTextData;
    private Spinner spinnerUm;
    private Spinner spinnerDetalheUm;
    private Spinner spinnerDetalheDois;
    private Spinner spinnerDetalheTres;
    private Spinner spinnerDetalheQuatro;
    private Spinner spinnerDois;
    private Spinner spinnerTres;
    private Spinner spinnerQuatro;
    private CheckBox checkAirBag;
    private CheckBox checkBoxAbs;
    private CheckBox checkBoxRoda;
    private CheckBox checkBoxPintura;
    private CheckBox checkBoxDirecao;
    private CheckBox checkBoxAr;
    private ArrayList<SpinnerComparacao> posicoes;
    private Button buttonEncontrar;
    private SpinnerComparacao spinnerComparacaoUm;
    private SpinnerComparacao spinnerComparacaoDois;
    private SpinnerComparacao spinnerComparacaoTres;
    private SpinnerComparacao spinnerComparacaoQuatro;
    private CarroDao carroDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparacao);
        carroDao = new CarroDao(getApplicationContext());
        atribuiViewsPeloID();
        defineSpinners();

        spinnerUm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerComparacao spinnerComparacao = buscaNovaPosicaoDoSpinner(spinnerComparacaoUm, position);
                spinnerComparacao.getSpinner().setSelection(spinnerComparacao.getPosicao());
                spinnerComparacaoUm.setPosicao(position);

                SpinnerFactory.criaSpinner(ComparacaoActivity.this, position, spinnerDetalheUm);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDois.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerComparacao spinnerComparacao = buscaNovaPosicaoDoSpinner(spinnerComparacaoDois, position);
                spinnerComparacao.getSpinner().setSelection(spinnerComparacao.getPosicao());
                spinnerComparacaoDois.setPosicao(position);
                SpinnerFactory.criaSpinner(ComparacaoActivity.this, position, spinnerDetalheDois);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerTres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerComparacao spinnerComparacao = buscaNovaPosicaoDoSpinner(spinnerComparacaoTres, position);
                spinnerComparacao.getSpinner().setSelection(spinnerComparacao.getPosicao());
                spinnerComparacaoTres.setPosicao(position);
                SpinnerFactory.criaSpinner(ComparacaoActivity.this, position, spinnerDetalheTres);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerQuatro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerComparacao spinnerComparacao = buscaNovaPosicaoDoSpinner(spinnerComparacaoQuatro, position);
                spinnerComparacao.getSpinner().setSelection(spinnerComparacao.getPosicao());
                spinnerComparacaoQuatro.setPosicao(position);
                SpinnerFactory.criaSpinner(ComparacaoActivity.this, position, spinnerDetalheQuatro);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editTextData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DataFragment().setCallback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar diaEscolhido = DateUtils.montaCalendar(dayOfMonth, monthOfYear, year);
                        editTextData.setText(DateUtils.getData(diaEscolhido.getTimeInMillis()));
                        editTextData.clearFocus();
                    }
                }).show(getSupportFragmentManager(), "Data");
            }
        });

        buttonEncontrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    comparaCarros();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

    private void comparaCarros() {
        try{
            Log.v("qual", "TO AKI ");
            Comparacao comparacao = defineComparacao();
            ArrayList<Carro> carros = carroDao.retornaTodosOsCarros();

            ArrayList<Ponto> pontos = new ArrayList<Ponto>();
            for(Carro carro : carros){
                Ponto ponto = new Ponto();
                ponto.setIdCarro(carro.getId());
                ponto.setPontos(comparacao.compara(carro));
                Log.v("qual", "Quantidade de pontos do carro " + carro.getModelo() + " = " + ponto.getPontos());
                pontos.add(ponto);
            }
            ResultadoDialogFragment resultadoDialogFragment = new ResultadoDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(ConstantsUtils.PONTOS, pontos);
            resultadoDialogFragment.setArguments(bundle);
            resultadoDialogFragment.show(getSupportFragmentManager(),"");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @NonNull
    private Comparacao defineComparacao() {
        Comparacao comparacao = new Comparacao();
        comparacao.setDataDaComparacao(editTextData.getText().toString());
        defineValoresDosSpinners(comparacao);
        defineValoresDosCheckBoxs(comparacao);
        return comparacao;
    }

    private void defineSpinners() {
        posicoes = new ArrayList<>();

        spinnerComparacaoUm = new SpinnerComparacao(ConstantsUtils.ANO, spinnerUm);
        spinnerComparacaoDois = new SpinnerComparacao(ConstantsUtils.FABRICANTE, spinnerDois);
        spinnerComparacaoTres = new SpinnerComparacao(ConstantsUtils.MODELO, spinnerTres);
        spinnerComparacaoQuatro = new SpinnerComparacao(ConstantsUtils.VALOR, spinnerQuatro);
        spinnerQuatro.setSelection(ConstantsUtils.VALOR);
        spinnerTres.setSelection(ConstantsUtils.MODELO);
        spinnerDois.setSelection(ConstantsUtils.FABRICANTE);
        posicoes.add(spinnerComparacaoUm);
        posicoes.add(spinnerComparacaoDois);
        posicoes.add(spinnerComparacaoTres);
        posicoes.add(spinnerComparacaoQuatro);
    }

    private void atribuiViewsPeloID() {
        editTextData = (EditText) findViewById(R.id.editTextData);
        spinnerUm = (Spinner) findViewById(R.id.spinnerUm);
        spinnerDetalheUm = (Spinner) findViewById(R.id.spinnerUmDetalhes);
        spinnerDetalheDois = (Spinner) findViewById(R.id.spinnerDoisDetalhes);
        spinnerDetalheTres = (Spinner) findViewById(R.id.spinnerTresDetalhes);
        spinnerDetalheQuatro = (Spinner) findViewById(R.id.spinnerQuatroDetalhes);
        spinnerDois = (Spinner) findViewById(R.id.spinnerDois);
        spinnerTres = (Spinner) findViewById(R.id.spinnerTres);
        spinnerQuatro = (Spinner) findViewById(R.id.spinnerQuatro);
        buttonEncontrar = (Button) findViewById(R.id.buttonEncontrar);
        checkAirBag = (CheckBox) findViewById(R.id.checkboxAirBag);
        checkBoxAbs = (CheckBox) findViewById(R.id.checkboxAbs);
        checkBoxPintura = (CheckBox) findViewById(R.id.checkboxPintura);
        checkBoxAr = (CheckBox) findViewById(R.id.checkboxArCondicionado);
        checkBoxDirecao = (CheckBox) findViewById(R.id.checkboxDirecao);
        checkBoxRoda = (CheckBox) findViewById(R.id.checkboxRodas);
    }

    private void defineValoresDosCheckBoxs(Comparacao comparacao) {
        comparacao.setAbs(checkBoxAbs.isChecked());
        comparacao.setAirbag(checkAirBag.isChecked());
        comparacao.setAr(checkBoxAr.isChecked());
        comparacao.setDirecao(checkBoxDirecao.isChecked());
        comparacao.setPintura(checkBoxPintura.isChecked());
        comparacao.setRodas(checkBoxRoda.isChecked());
    }

    private void defineValoresDosSpinners(Comparacao comparacao) {
        String primeiroSpinner = ((String) spinnerUm.getSelectedItem());
        String primeiroResultado = ((String) spinnerDetalheUm.getSelectedItem());

        String segundoSpinner = ((String) spinnerDois.getSelectedItem());
        String segundoResultado = ((String) spinnerDetalheDois.getSelectedItem());

        String terceiroSpinner = ((String) spinnerTres.getSelectedItem());
        String terceiroResultado = ((String) spinnerDetalheTres.getSelectedItem());

        String quartoSpinner = ((String) spinnerQuatro.getSelectedItem());
        String quartoResultado = ((String) spinnerDetalheQuatro.getSelectedItem());

        salvaValoresDosSpinners(comparacao, primeiroSpinner, primeiroResultado);
        salvaValoresDosSpinners(comparacao, segundoSpinner, segundoResultado);
        salvaValoresDosSpinners(comparacao, terceiroSpinner, terceiroResultado);
        salvaValoresDosSpinners(comparacao, quartoSpinner, quartoResultado);
    }

    private void salvaValoresDosSpinners(Comparacao comparacao, String spinnerDecisao, String spinnerDetalhe) {
        String spinner = spinnerDecisao;
        String spinnerResultado = spinnerDetalhe;

        if(spinner.equals(getString(R.string.ano))){
            if (spinnerResultado.equals(getString(R.string.mais_antigo))) {
                comparacao.setMaisNovo(false);
                comparacao.setMaisVelho(true);
            } else {
                comparacao.setMaisNovo(true);
                comparacao.setMaisVelho(false);
            }
        }else if(spinner.equals(getString(R.string.fabricante))){
            comparacao.setFabricante(spinnerResultado);
        }else if(spinner.equals(getString(R.string.modelo))){
            comparacao.setModelo(spinnerResultado);
        }else{
            if(spinnerResultado.equals(getString(R.string.mais_barato))){
                comparacao.setMaisBarato(true);
                comparacao.setMaisCaro(false);
            }else{
                comparacao.setMaisBarato(false);
                comparacao.setMaisCaro(true);
            }
        }
    }

    private SpinnerComparacao buscaNovaPosicaoDoSpinner(SpinnerComparacao spinnerComparacao, int novaPosicao) {
        int posicaoAntiga = spinnerComparacao.getPosicao();
        for (SpinnerComparacao spinner : posicoes) {
            if (spinner.getPosicao() == novaPosicao) {
                spinner.setPosicao(posicaoAntiga);
                return spinner;
            }
        }
        return spinnerComparacao;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_comparacao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

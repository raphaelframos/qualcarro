package powell.com.qualcarro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import powell.com.adapter.CarroAdapter;
import powell.com.dao.CarroDao;
import powell.com.model.BancoSingleton;
import powell.com.model.Carro;
import powell.com.utils.ConstantsUtils;

public class CarrosActivity extends AppCompatActivity {

    private ListView listaDeCarros;
    private ArrayList<Carro> carros;
    private CarroAdapter carroAdapter;
    private CarroDao carroDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carros);
        atribuiViews();
        inicioDados();

        carros = carroDao.retornaTodosOsCarros();
        carroAdapter = new CarroAdapter(getApplicationContext(),carros);
        listaDeCarros.setAdapter(carroAdapter);
        listaDeCarros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getApplicationContext(), CarroEscolhidoActivity.class);
                Carro carro = (Carro) listaDeCarros.getItemAtPosition(position);
                it.putExtra(ConstantsUtils.CARRO_ESCOLHIDO, carro);
                startActivity(it);
                Toast.makeText(getApplicationContext(),"Cliquei no " + carros.get(position),Toast.LENGTH_LONG).show();
            }
        });



    }

    private void inicioDados() {
        carroDao = new CarroDao(getApplicationContext());
    }

    private void atribuiViews() {
        listaDeCarros = (ListView) findViewById(R.id.listaDeCarros);
    }

    @Override
    public void onResume(){
        super.onResume();

        carros = carroDao.retornaTodosOsCarros();
        carroAdapter = new CarroAdapter(getApplicationContext(),carros);
        listaDeCarros.setAdapter(carroAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_carros, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_novo_caro) {
            startActivity(new Intent(CarrosActivity.this, NovoCarroActivity.class));
            return true;
        }

        if(id == R.id.action_comparacao){
            startActivity(new Intent(CarrosActivity.this, ComparacaoActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

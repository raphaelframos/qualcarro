package powell.com.qualcarro;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class EntradaActivity extends AppCompatActivity {

    private final static int TEMPO_ENTRADA = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(EntradaActivity.this, CalculadoraActivity.class));
                finish();
            }
        }, TEMPO_ENTRADA);
    }
}

package powell.com.qualcarro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;

import powell.com.dao.CarroDao;
import powell.com.model.Carro;
import powell.com.utils.ConstantsUtils;

public class CarroEscolhidoActivity extends AppCompatActivity {

    private Spinner spinnerFabricante;
    private EditText editTextModelo;
    private EditText editTextAno;
    private EditText editTextValor;
    private CheckBox checkAirBag;
    private CheckBox checkBoxAbs;
    private CheckBox checkBoxRoda;
    private CheckBox checkBoxPintura;
    private CheckBox checkBoxDirecao;
    private CheckBox checkBoxAr;
    private Button buttonSalvar;
    private Carro carro;
    private CarroDao carroDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carro_escolhido);
        carro = (Carro) getIntent().getSerializableExtra(ConstantsUtils.CARRO_ESCOLHIDO);

        spinnerFabricante = (Spinner) findViewById(R.id.spinnerFabricante);
        editTextModelo = (EditText) findViewById(R.id.editTextModelo);
        editTextAno = (EditText) findViewById(R.id.editTextAno);
        editTextValor = (EditText) findViewById(R.id.editTextValor);
        checkAirBag = (CheckBox) findViewById(R.id.checkboxAirBag);
        checkBoxAbs = (CheckBox) findViewById(R.id.checkboxAbs);
        checkAirBag = (CheckBox) findViewById(R.id.checkboxAirBag);
        checkBoxAr = (CheckBox) findViewById(R.id.checkboxArCondicionado);
        checkBoxDirecao = (CheckBox) findViewById(R.id.checkboxDirecao);
        checkBoxRoda = (CheckBox) findViewById(R.id.checkboxRodas);
        checkBoxPintura = (CheckBox) findViewById(R.id.checkboxPintura);
        buttonSalvar = (Button) findViewById(R.id.buttonSalvar);

        carroDao = new CarroDao(getApplicationContext());

        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fabricante = (String) spinnerFabricante.getSelectedItem();
                String modelo = editTextModelo.getEditableText().toString();
                String ano = editTextAno.getEditableText().toString();
                boolean temAirBag = checkAirBag.isChecked();
                boolean isAbs = checkBoxAbs.isChecked();

                if (validaCampos(modelo, ano)) {
                    carro.setFabricante(fabricante);
                    carro.setModelo(modelo);
                    carro.setAno(Integer.valueOf(ano));
                    carro.setAbs(isAbs);
                    carro.setAirbag(temAirBag);

                    carroDao.adiciona(carro);
                    finish();
                }
            }
        });

        mostraValores(carro);

        modificaViews(false);

    }

    private boolean validaCampos(String modelo, String ano) {
        if(modelo.equals("")){
            editTextModelo.setError(getString(R.string.campo_branco));
            return false;
        }

        if(ano.equals("")){
            editTextAno.setError(getString(R.string.campo_branco));
            return false;
        }

        return true;
    }

    private void mostraValores(Carro carro) {
        editTextAno.setText(carro.getAno()+"");
        editTextModelo.setText(carro.getModelo());
        spinnerFabricante.setSelection(definePosicaoDoFabricante(carro.getFabricante()));
        checkAirBag.setChecked(carro.isAirbag());
        checkBoxAbs.setChecked(carro.isAbs());
    }

    private int definePosicaoDoFabricante(String marca) {
        String[] vetorDeFabricantes = getResources().getStringArray(R.array.fabricantes);
        ArrayList<String> fabricantes = new ArrayList<String>();
        fabricantes.addAll(Arrays.asList(vetorDeFabricantes));
        return fabricantes.indexOf(marca);
    }

    private void modificaViews(boolean valor) {
        spinnerFabricante.setEnabled(valor);
        editTextAno.setEnabled(valor);
        editTextModelo.setEnabled(valor);
        editTextValor.setEnabled(valor);
        checkAirBag.setEnabled(valor);
        checkBoxAbs.setEnabled(valor);
        checkBoxAr.setEnabled(valor);
        checkBoxDirecao.setEnabled(valor);
        checkBoxPintura.setEnabled(valor);
        checkBoxRoda.setEnabled(valor);
        if(valor){
            buttonSalvar.setVisibility(View.VISIBLE);
        }else{
            buttonSalvar.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_carro_escolhido, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_editar) {
            modificaViews(true);
            return true;
        }else if (id == R.id.action_remover) {
            carroDao.remove(carro.getId());
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

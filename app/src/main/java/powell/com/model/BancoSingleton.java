package powell.com.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by raphael on 03/09/15.
 */
public class BancoSingleton {

    private static BancoSingleton instance;
    private static ArrayList<Carro> carros;

    public BancoSingleton(){

    }

    public static BancoSingleton getInstance(){
        if(instance == null){
            return new BancoSingleton();
        }else{
            return instance;
        }
    }

    public void adiciona(Carro carro){
        if(carros == null){
            carros = new ArrayList<>();
        }
        this.carros.add(carro);
    }

    public ArrayList<Carro> getCarros() {
        if(carros == null){
            return new ArrayList<>();
        }
        return carros;
    }
}

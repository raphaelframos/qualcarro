package powell.com.model;

import android.content.ContentValues;

import java.io.Serializable;
import java.math.BigDecimal;

import powell.com.utils.ConstantsUtils;

/**
 * Created by raphael on 03/09/15.
 */
public class Carro implements Serializable{

    private Integer id;
    private String fabricante;
    private String modelo;
    private int ano;
    private boolean abs;
    private boolean airbag;
    private boolean direcao;
    private boolean pintura;
    private boolean ar;
    private boolean roda;
    private BigDecimal valor;



    public Carro(){}

    public Carro(int id, String fabricante, String modelo, int ano){
        setId(id);
        setFabricante(fabricante);
        setAno(ano);
        setModelo(modelo);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString(){
        return "Nome " + getModelo() + " - Marca " + getFabricante() + " de " + getAno();
    }

    public ContentValues getValues() {
        ContentValues values = new ContentValues();
        values.put(ConstantsUtils.CARRO_ID, id);
        values.put(ConstantsUtils.CARRO_ANO, ano);
        values.put(ConstantsUtils.CARRO_MARCA, fabricante);
        values.put(ConstantsUtils.CARRO_NOME, modelo);
        values.put(ConstantsUtils.CARRO_ABS, abs);
        values.put(ConstantsUtils.CARRO_AIRBAG, airbag);
        values.put(ConstantsUtils.CARRO_AR, ar);
        values.put(ConstantsUtils.CARRO_DIRECAO, direcao);
        values.put(ConstantsUtils.CARRO_PINTURA, pintura);
        values.put(ConstantsUtils.CARRO_RODA, roda);
        return values;
    }

    public boolean isAbs() {
        return abs;
    }

    public void setAbs(boolean abs) {
        this.abs = abs;
    }

    public boolean isAirbag() {
        return airbag;
    }

    public void setAirbag(boolean airbag) {
        this.airbag = airbag;
    }

    public void setAirbag(int airbagInteiro) {
        if(airbagInteiro == 0){
            setAirbag(false);
        }else{
            setAirbag(true);
        }
    }

    public void setAbs(int absInteiro) {
        if(absInteiro == 0){
            setAbs(false);
        }else{
            setAbs(true);
        }
    }

    public boolean isDirecao() {
        return direcao;
    }

    public void setDirecao(boolean direcao) {
        this.direcao = direcao;
    }

    public boolean isPintura() {
        return pintura;
    }

    public void setPintura(boolean pintura) {
        this.pintura = pintura;
    }

    public boolean isAr() {
        return ar;
    }

    public void setAr(boolean ar) {
        this.ar = ar;
    }

    public boolean isRoda() {
        return roda;
    }

    public void setRoda(boolean roda) {
        this.roda = roda;
    }

    public void setAr(int ar) {
        if(ar == 0){
            setAr(false);
        }else{
            setAr(true);
        }
    }

    public void setDirecao(int direcao) {
        if(direcao == 0){
            setDirecao(false);
        }else{
            setDirecao(true);
        }
    }

    public void setPintura(int pintura) {
        if(pintura == 0){
            setPintura(false);
        }else{
            setPintura(true);
        }
    }

    public void setRoda(int roda) {
        if(roda == 0){
            setRoda(false);
        }else{
            setRoda(true);
        }
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public void setValor(String valorTexto) {
        try{
            setValor(new BigDecimal(valorTexto));
        }catch (Exception e){
            setValor(BigDecimal.ZERO);
        }
    }

    public void setAno(String ano) {
        try{
            setAno(Integer.valueOf(ano));
        }catch (Exception e){
            setAno(0);
        }
    }
}

package powell.com.model;

/**
 * Created by raphael on 29/10/15.
 */
public class Ponto {

    private int idCarro;
    private int pontos;

    public int getIdCarro() {
        return idCarro;
    }

    public void setIdCarro(int idCarro) {
        this.idCarro = idCarro;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
}

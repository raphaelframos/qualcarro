package powell.com.model;

import android.widget.Spinner;

/**
 * Created by raphael on 20/10/15.
 */
public class SpinnerComparacao {

    private int posicao;
    private Spinner spinner;
    public SpinnerComparacao(int posicao, Spinner spinner) {
        this.posicao = posicao;
        this.spinner = spinner;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSpinner(Spinner spinner) {
        this.spinner = spinner;
    }

    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }
}

package powell.com.model;

/**
 * Created by raphael on 28/10/15.
 */
public class Comparacao {

    private boolean maisBarato;
    private boolean maisCaro;
    private String fabricante;
    private String modelo;
    private boolean maisNovo;
    private boolean maisVelho;
    private boolean abs;
    private boolean airbag;
    private boolean ar;
    private boolean direcao;
    private boolean pintura;
    private boolean rodas;
    private String dataDaComparacao;


    @Override
    public String toString(){
        return "Mais barato - " + maisBarato + " Mais caro " + maisCaro + " Fabricante - " + fabricante + " modelo - " + modelo + "\nmais Novo - " + maisNovo + " maisVelho " +
                maisVelho + " abs " + abs + " air " + airbag + " ar - " + ar + "\ndirecao " + direcao + " pintura " + pintura + " rodas " + rodas + " data " + dataDaComparacao;
    }

    public boolean isMaisBarato() {
        return maisBarato;
    }

    public void setMaisBarato(boolean maisBarato) {
        this.maisBarato = maisBarato;
    }

    public boolean isMaisCaro() {
        return maisCaro;
    }

    public void setMaisCaro(boolean maisCaro) {
        this.maisCaro = maisCaro;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isMaisNovo() {
        return maisNovo;
    }

    public void setMaisNovo(boolean maisNovo) {
        this.maisNovo = maisNovo;
    }

    public boolean isMaisVelho() {
        return maisVelho;
    }

    public void setMaisVelho(boolean maisVelho) {
        this.maisVelho = maisVelho;
    }

    public boolean isAbs() {
        return abs;
    }

    public void setAbs(boolean abs) {
        this.abs = abs;
    }

    public boolean isAirbag() {
        return airbag;
    }

    public void setAirbag(boolean airbag) {
        this.airbag = airbag;
    }

    public boolean isAr() {
        return ar;
    }

    public void setAr(boolean ar) {
        this.ar = ar;
    }

    public boolean isDirecao() {
        return direcao;
    }

    public void setDirecao(boolean direcao) {
        this.direcao = direcao;
    }

    public boolean isPintura() {
        return pintura;
    }

    public void setPintura(boolean pintura) {
        this.pintura = pintura;
    }

    public boolean isRodas() {
        return rodas;
    }

    public void setRodas(boolean rodas) {
        this.rodas = rodas;
    }

    public String getDataDaComparacao() {
        return dataDaComparacao;
    }

    public void setDataDaComparacao(String dataDaComparacao) {
        this.dataDaComparacao = dataDaComparacao;
    }


    public int compara(Carro carro) {
        int pontos = 0;

        if(isAbs() && carro.isAbs()){
            pontos++;
        }else if(!isAbs() && !carro.isAbs()){
            pontos++;
        }

        if(isAirbag() && carro.isAirbag()){
            pontos++;
        }else if(!isAirbag() && !carro.isAirbag()){
            pontos++;
        }

        if(isAr()&& carro.isAr()){
            pontos++;
        }else if(!isAr() && !carro.isAr()){
            pontos++;
        }

        if(isDirecao() && carro.isDirecao()){
            pontos++;
        }else if(!isDirecao() && !carro.isDirecao()){
            pontos++;
        }

        if(isPintura() && carro.isPintura()){
            pontos++;
        }else if(!isPintura() && !carro.isPintura()){
            pontos++;
        }

        if(isRodas() && carro.isRoda()){
            pontos++;
        }else if(!isRodas() && !carro.isRoda()){
            pontos++;
        }


        return pontos;
    }
}

package powell.com.padroes;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;

import powell.com.dao.CarroDao;
import powell.com.qualcarro.ComparacaoActivity;
import powell.com.qualcarro.R;
import powell.com.utils.ConstantsUtils;

/**
 * Created by raphael on 26/10/15.
 */
public class SpinnerFactory {

    public static ArrayList<String> getOpcoesDeAno(Context context){
        ArrayList<String> opcoes = new ArrayList<>();
        String[] vetor =  context.getResources().getStringArray(R.array.spinner_ano);
        opcoes.addAll(Arrays.asList(vetor));
        return opcoes;
    }

    public static void criaSpinner(Activity activity, int posicao, Spinner spinner) {

        ArrayList<String> opcoes = new ArrayList<>();

        switch (posicao){
            case ConstantsUtils.ANO:
                opcoes = getOpcoesDeAno(activity);
                break;

            case ConstantsUtils.FABRICANTE:
                opcoes = getOpcoesDeFabricante(activity);
                break;

            case ConstantsUtils.MODELO:
                opcoes = getOpcoesDeModelo(activity);
                break;

            case ConstantsUtils.VALOR:
                opcoes = getOpcoesDeValor(activity);
                break;

        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, opcoes);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    private static ArrayList<String> getOpcoesDeValor(Activity activity) {
        ArrayList<String> opcoes = new ArrayList<>();
        String[] vetor =  activity.getResources().getStringArray(R.array.spinner_valor);
        opcoes.addAll(Arrays.asList(vetor));
        return opcoes;
    }

    private static ArrayList<String> getOpcoesDeFabricante(Activity activity) {
        ArrayList<String> opcoes = new ArrayList<>();
        String[] vetor =  activity.getResources().getStringArray(R.array.fabricantes);
        opcoes.addAll(Arrays.asList(vetor));
        return opcoes;
    }

    private static ArrayList<String> getOpcoesDeModelo(Activity activity) {
        CarroDao carroDao = new CarroDao(activity);
        return carroDao.encontraModelos();
    }
}

package powell.com.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by raphael on 03/04/15.
 */
public class DateUtils {

    public static String getHorario(Long data) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date = new Date(data);
        String tempoDemonstracao = format.format(date);
        return tempoDemonstracao;
    }

    public static String getHorario(Calendar calendar){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date = new Date(calendar.getTimeInMillis());
        String tempoDemonstracao = format.format(date);
        return tempoDemonstracao;
    }

    public static String getDataComHora(long data){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm");
        Date date = new Date(data);
        String dataDemonstracao = format.format(date);
        return dataDemonstracao + "h";
    }

    public static String getData(Long data) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
        Date date = new Date(data);
        return format.format(date);
    }

    public static String getDateHourServer(long data){
        if(data == 0){
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date(data);
        String dataDemonstracao = format.format(date);
        return dataDemonstracao;
    }

    public static Long getDataHoje() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static boolean isVencida(Calendar calendar) {
        return calendar.compareTo(Calendar.getInstance()) <= 0;
    }

    public static Calendar retornaCalendarComDataAtual() {
        return Calendar.getInstance();
    }

    public static Calendar montaCalendar(int dia, int mes, int ano) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,dia);
        calendar.set(Calendar.MONTH, mes);
        calendar.set(Calendar.YEAR, ano);
        return calendar;
    }
}

package powell.com.utils;

/**
 * Created by raphael on 17/09/15.
 */
public class ConstantsUtils {

    public static final int VERSAO_BANCO = 1;

    public static final String TABELA_CARRO = "CARRO";
    public static final String CARRO_ID = "ID";
    public static final String CARRO_MARCA = "MARCA";
    public static final String CARRO_NOME = "NOME";
    public static final String CARRO_ANO = "ANO";
    public static final String CARRO_ABS = "ABS";
    public static final String CARRO_AIRBAG = "AIRBAG";
    public static final String CARRO_AR = "AR";
    public static final String CARRO_DIRECAO = "DIRECAO";
    public static final String CARRO_RODA = "RODA";
    public static final String CARRO_PINTURA = "PINTURA";


    public static final String CRIA_TABELA_CARRO = "CREATE TABLE " + TABELA_CARRO + " (" +
            CARRO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CARRO_NOME + " TEXT, " + CARRO_MARCA + " TEXT, " +
            CARRO_ANO + " INTEGER, " + CARRO_ABS + " BOOLEAN, " + CARRO_AIRBAG + " BOOLEAN, " +
            CARRO_AR + " BOOLEAN, " + CARRO_DIRECAO + " BOOLEAN, " + CARRO_RODA + " BOOLEAN, " + CARRO_PINTURA + " BOOLEAN)";
    public static final String CARRO_ESCOLHIDO = "carro";
    public static final int ANO = 0;
    public static final int FABRICANTE = 1;
    public static final int MODELO = 2;
    public static final int VALOR = 3;
    public static final String PONTOS = "PONTOS";


    public static String NOME_BANCO = "qualcarro.db";
}

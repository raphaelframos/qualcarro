package powell.com.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import powell.com.dao.CarroDao;
import powell.com.model.Carro;
import powell.com.model.CarroComparado;
import powell.com.model.Ponto;
import powell.com.qualcarro.R;
import powell.com.utils.ConstantsUtils;

/**
 * Created by raphael on 05/01/16.
 */
public class ResultadoDialogFragment extends DialogFragment{

    private ArrayList<Ponto> pontos;
    private CarroDao carroDao;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_resultado, container);
        listView = (ListView) view.findViewById(R.id.listViewCarrosComparados);

        carroDao = new CarroDao(getActivity());
        pontos = (ArrayList<Ponto>) getArguments().getSerializable(ConstantsUtils.PONTOS);

        for(Ponto ponto : pontos){
            Carro carro = carroDao.retornaCarroPeloId(ponto.getIdCarro());
            CarroComparado carroComparado = new CarroComparado();

        }

        return view;
    }
}

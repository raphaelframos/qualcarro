package powell.com.fragments;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import powell.com.utils.DateUtils;

public class DataFragment extends DialogFragment{
	
	private OnDateSetListener onDate;
	
	public DataFragment(){}
	
	@Override
	public Dialog onCreateDialog(Bundle bundle){
		Calendar calendar = DateUtils.retornaCalendarComDataAtual();
		return new DatePickerDialog(getActivity(), onDate, calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
	}

	public DialogFragment setCallback(OnDateSetListener onDate) {
		this.onDate = onDate;
		return this;
	}

}

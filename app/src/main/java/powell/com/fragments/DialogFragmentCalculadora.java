package powell.com.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;

import powell.com.qualcarro.R;

/**
 * Created by raphael on 18/03/16.
 */
public class DialogFragmentCalculadora extends DialogFragment {

    private TextView textViewResultado;
    private EditText editTextNumero;
    private Button buttonSomar;
    private Button buttonDiminuir;
    private Button buttonMultiplicar;
    private Button buttonDividir;
    private Button buttonResultado;
    private BigDecimal primeiroNumero;
    private BigDecimal segundoNumero;
    private String sinalDaOperacao;
    private BigDecimal resultado;
    private Button buttonC;
    private Button buttonFinanca;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_calculadora, container);
        textViewResultado = (TextView) view.findViewById(R.id.textViewResultado);
        editTextNumero = (EditText) view.findViewById(R.id.editTextNumero);
        buttonDiminuir = (Button) view.findViewById(R.id.buttonDiminuir);
        buttonDividir = (Button) view.findViewById(R.id.buttonDividir);
        buttonMultiplicar = (Button) view.findViewById(R.id.buttonMultiplicar);
        buttonSomar = (Button) view.findViewById(R.id.buttonSomar);
        buttonResultado = (Button) view.findViewById(R.id.buttonIgual);
        buttonC = (Button) view.findViewById(R.id.buttonC);
        buttonFinanca = (Button) view.findViewById(R.id.buttonF);

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reiniciaCalculadora();
            }
        });

        buttonFinanca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reiniciaCalculadora();
                primeiroNumero = new BigDecimal(100);
            }
        });

        buttonSomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaOperacaoEPrimeiroNumero(buttonSomar);
            }
        });

        buttonDiminuir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaOperacaoEPrimeiroNumero(buttonDiminuir);
            }
        });

        buttonDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaOperacaoEPrimeiroNumero(buttonDividir);
            }
        });

        buttonMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaOperacaoEPrimeiroNumero(buttonMultiplicar);
            }
        });

        buttonResultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    segundoNumero = new BigDecimal(editTextNumero.getText().toString());
                }catch (Exception e){
                    e.printStackTrace();
                    segundoNumero = BigDecimal.ZERO;
                }

                calcula();
                editTextNumero.setText("");
                if(resultado != null){
                    textViewResultado.setText(textViewResultado.getText().toString() + " " + segundoNumero.toString() + " = " + resultado.toString());
                }
                reiniciaCalculadora();
            }
        });
        return view;
    }

    private void capturaOperacaoEPrimeiroNumero(Button button) {
        try{
            primeiroNumero = new BigDecimal(editTextNumero.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
            primeiroNumero = BigDecimal.ZERO;
        }

        sinalDaOperacao = button.getText().toString();

        textViewResultado.setText(primeiroNumero.toString() + " " + sinalDaOperacao);
        editTextNumero.setText("");
    }

    private void reiniciaCalculadora() {
        resultado = null;
        primeiroNumero = null;
        segundoNumero = null;
    }

    private void calcula() {
        if(primeiroNumero != null){
            if(sinalDaOperacao.equals("+")){
                resultado = primeiroNumero.add(segundoNumero);
            }else if(sinalDaOperacao.equals("-")){
                resultado = primeiroNumero.subtract(segundoNumero);
            }else if(sinalDaOperacao.equals("/")){
                resultado = primeiroNumero.divide(segundoNumero, BigDecimal.ROUND_CEILING);
            }else if(sinalDaOperacao.equals("*")){
                resultado = primeiroNumero.multiply(segundoNumero);
            }else if(sinalDaOperacao.equals("^")){
                resultado = primeiroNumero.multiply(primeiroNumero);
            }if(sinalDaOperacao.equals("%")){

            }
        }
    }
}

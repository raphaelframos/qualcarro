package powell.com.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import powell.com.utils.ConstantsUtils;

/**
 * Created by raphael on 17/09/15.
 */
public class AssistenteDoBanco extends SQLiteOpenHelper {

    public AssistenteDoBanco(Context context) {
        super(context, ConstantsUtils.NOME_BANCO, null, ConstantsUtils.VERSAO_BANCO);
        Log.d("", "Cria banco");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ConstantsUtils.CRIA_TABELA_CARRO);
        Log.d("", "Cria TABELA");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

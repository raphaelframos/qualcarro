package powell.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import powell.com.model.Carro;
import powell.com.qualcarro.R;

/**
 * Created by raphael on 10/09/15.
 */
public class CarroAdapter extends ArrayAdapter<Carro> {

    private ArrayList<Carro> carros;

    //Decorado
    public CarroAdapter(Context context, ArrayList<Carro> carros) {
        super(context, R.layout.activity_carros, carros);
        this.carros = carros;
    }

    //Decorado
    @Override
    public View getView(int posicao, View view, ViewGroup parent){

        CarroViewHolder carroViewHolder = null;
        //Decorado
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.adapter_carro, parent, false);
            carroViewHolder = new CarroViewHolder(view);
            view.setTag(carroViewHolder);
        }else{
            carroViewHolder = (CarroViewHolder) view.getTag();
        }

        Carro carro = carros.get(posicao);
        carroViewHolder.textViewNome.setText(carro.getModelo());
        carroViewHolder.textMarca.setText(carro.getFabricante());
        carroViewHolder.textAno.setText(carro.getAno() + "");


        return view;
    }

    public class CarroViewHolder{
        TextView textViewNome;
        TextView textMarca;
        TextView textAno;

        public CarroViewHolder(View view) {
            textViewNome = (TextView) view.findViewById(R.id.textViewNome);
            textAno = (TextView) view.findViewById(R.id.textViewAno);
            textMarca = (TextView) view.findViewById(R.id.textViewMarca);
        }
    }


}

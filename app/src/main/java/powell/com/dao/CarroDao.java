package powell.com.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import powell.com.model.Carro;
import powell.com.utils.ConstantsUtils;

/**
 * Created by raphael on 17/09/15.
 */
public class CarroDao extends AbstractDao {

    public CarroDao(Context context) {
        super(context);
    }

    public void adiciona(Carro carro){
        if(existe(carro.getId())){
            Log.d("", "Atualiza carro " + carro.toString());
            getBancoParaEscrita().update(ConstantsUtils.TABELA_CARRO,carro.getValues(),ConstantsUtils.CARRO_ID + " = ?",new String[] {carro.getId().toString()});
            Log.d("", "Atualiza carro");
        }else{
            getBancoParaEscrita().insert(ConstantsUtils.TABELA_CARRO, null, carro.getValues());
            Log.d("", "Adiciona carro");
        }

    }

    private boolean existe(Integer id) {
        try{
            String[] parametro = {id.toString()};
            Cursor cursorComCarro = getBancoParaLeitura().query(ConstantsUtils.TABELA_CARRO,null,ConstantsUtils.CARRO_ID + " = ?",parametro,null,null,null,null);
            boolean existeCarro = cursorComCarro.getCount() > 0;
            cursorComCarro.close();
            return existeCarro;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public Carro retornaPrimeiroCarroInserido(){
        Cursor cursor = getBancoParaLeitura().query(ConstantsUtils.TABELA_CARRO,null,null,null,null,null,null);
        cursor.moveToFirst();
        Carro carro = criaCarroAPartirDo(cursor);
        cursor.close();
        return carro;
    }

    public Carro criaCarroAPartirDo(Cursor cursor){
        Carro carro = new Carro();
        carro.setAno(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_ANO)));
        carro.setFabricante(cursor.getString(cursor.getColumnIndex(ConstantsUtils.CARRO_MARCA)));
        carro.setId(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_ID)));
        carro.setModelo(cursor.getString(cursor.getColumnIndex(ConstantsUtils.CARRO_NOME)));
        carro.setAirbag(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_AIRBAG)));
        carro.setAbs(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_ABS)));
        carro.setAr(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_AR)));
        carro.setDirecao(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_DIRECAO)));
        carro.setPintura(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_PINTURA)));
        carro.setRoda(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.CARRO_RODA)));
        return carro;
    }

    public ArrayList<Carro> retornaTodosOsCarros() {
        ArrayList<Carro> carros = new ArrayList<>();
        Cursor cursor = getBancoParaLeitura().query(ConstantsUtils.TABELA_CARRO,null,null,null,null,null,null);
        while(cursor.moveToNext()){
            Carro carro = criaCarroAPartirDo(cursor);
            carros.add(carro);
        }
        cursor.close();
        return carros;
    }

    public void remove(Integer id) {
        getBancoParaEscrita().delete(ConstantsUtils.TABELA_CARRO,ConstantsUtils.CARRO_ID + " = ?",new String[] {id.toString()});
        Log.d("", "Remove carro");
    }

    public ArrayList<String> encontraModelos() {
        ArrayList<String> modelos = new ArrayList<>();
        Cursor cursor = getBancoParaLeitura().query(ConstantsUtils.TABELA_CARRO,new String[] {ConstantsUtils.CARRO_NOME},null,null,null,null,null);
        while(cursor.moveToNext()){
            modelos.add(cursor.getString(0));
        }
        cursor.close();
        return modelos;
    }

    public Carro retornaCarroPeloId(Integer idCarro) {
        String[] parametro = {idCarro.toString()};
        Cursor cursor = getBancoParaLeitura().query(ConstantsUtils.TABELA_CARRO,null,ConstantsUtils.CARRO_ID + " = ?",parametro,null,null,null);
        cursor.moveToFirst();
        Carro carro = criaCarroAPartirDo(cursor);
        cursor.close();
        return carro;
    }
}

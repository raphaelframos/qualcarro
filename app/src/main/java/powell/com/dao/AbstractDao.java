package powell.com.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import powell.com.db.AssistenteDoBanco;

/**
 * Created by raphael on 17/09/15.
 */
public abstract class AbstractDao {

    private AssistenteDoBanco assistenteDoBanco;

    public AbstractDao(Context context){
        assistenteDoBanco = new AssistenteDoBanco(context);
    }

    public SQLiteDatabase getBancoParaEscrita(){
        return assistenteDoBanco.getWritableDatabase();
    }

    public SQLiteDatabase getBancoParaLeitura(){
        return assistenteDoBanco.getReadableDatabase();
    }

    public void fechaBanco(){
        try{
            assistenteDoBanco.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
